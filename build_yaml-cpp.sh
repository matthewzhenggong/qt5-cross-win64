#!/bin/bash

wget https://github.com/jbeder/yaml-cpp/archive/yaml-cpp-0.6.3.tar.gz
tar -xzf yaml-cpp-0.6.3.tar.gz
rm yaml-cpp-0.6.3.tar.gz

cd yaml-cpp-yaml-cpp-0.6.3
mkdir build
cd build

cmake .. -DCMAKE_INSTALL_PREFIX=/opt/win64 -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=/opt/win64/x86_64-mingw-w64-toolchain.cmake -DYAML_BUILD_SHARED_LIBS=ON -DYAML_CPP_BUILD_TESTS=OFF -DYAML_CPP_BUILD_TOOLS=OFF
make
make install

cd ../..
rm -rf yaml-cpp-yaml-cpp-0.6.3
