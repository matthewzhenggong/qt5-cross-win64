#!/bin/bash

basedir=$(dirname "$(readlink -f "$0")")
log_file="/build/build.log"

if [ "$#" -ne 1 ]; then
    echo "Usage: $0 [qt version]"
    exit 1
fi

qt_version="$1"
qt_version_parts=(${qt_version//./ })
qt_maj_minor="${qt_version_parts[0]}.${qt_version_parts[1]}"

qt_base_name="qt-everywhere-src"
if [ ${qt_version_parts[1]} -lt 10 ]; then
    qt_base_name="qt-everywhere-opensource-src"
fi

qt_source_dir="$qt_base_name-$qt_version"
qt_source_file="$qt_base_name-$qt_version.tar.xz"
qt_url="http://download.qt.io/official_releases/qt/$qt_maj_minor/$qt_version/single/$qt_source_file"

if [ ! -d "$qt_source_dir" ]; then
    if [ ! -f "$qt_source_file" ]; then
        echo "Downloading Qt sources from $qt_url ..."
        wget -nv "$qt_url"
    fi
    echo "Extracting Qt sources"
    tar -xJf "$qt_source_file"
    pushd "$qt_source_dir"
else
    pushd "$qt_source_dir"
fi

# Configure Qt for Android
# Skip bits we definitely do not need
# Disable mapbox related stuff because it fails to build with clang
# Disable statx usage since it causes failures when building in Docker
echo "Configuring Qt..."
./configure \
    -prefix /opt/win64 \
    -opensource \
    -confirm-license \
    -release \
    -xplatform win32-g++ \
    -device-option CROSS_COMPILE=x86_64-w64-mingw32- \
    -opengl desktop \
    -nomake tests \
    -nomake examples \
    -skip qt3d \
    -skip qtactiveqt \
    -skip qtcharts \
    -skip qtconnectivity \
    -skip qtdatavis3d \
    -skip qtdoc \
    -skip qtgamepad \
    -skip qtimageformats \
    -skip qtlocation \
    -skip qtlottie \
    -skip qtmultimedia \
    -skip qtnetworkauth \
    -skip qtpurchasing \
    -skip qtquick3d \
    -skip qtquickcontrols \
    -skip qtquicktimeline \
    -skip qtremoteobjects \
    -skip qtscript \
    -skip qtscxml \
    -skip qtsensors \
    -skip qtserialbus \
    -skip qtserialport \
    -skip qtspeech \
    -skip qttools \
    -skip qttranslations \
    -skip qtvirtualkeyboard \
    -skip qtwayland \
    -skip qtwebchannel \
    -skip qtwebengine \
    -skip qtwebglplugin \
    -skip qtwebsockets \
    -skip qtwebview \
    -skip qtxmlpatterns \
    -no-dbus \
    > $log_file

echo "Building Qt..."
make > $log_file
make install > $log_file

popd
rm -r "$qt_source_dir"
rm "$qt_source_file"
